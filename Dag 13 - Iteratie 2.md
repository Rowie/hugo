---
Title: Dag 13 - Iteratie 2
Date: 2017-09-20
---
Vandaag zijn we begonnen met de deliverables van iteratie 2. We hebben dus erg zelfstandig gewerkt. Ik ben begonnen met onderzoek naar de doelgroep, ook heb ik samen met een teamgenoot een andere enquête gemaakt om meer te weten te komen over wat de interesses zijn van CMD'ers. Verder heb ik wat verbeterd in mijn moodboard. 

Na de lunch heb ik een workshop gehad over *Brainstormen*.  Hier ging het over creatieve technieken toepassen, en de techniek die we toegepast hebben heet **de groen en rood licht fase**. 

Later deze middag hebben we weer les gehad van de coach. Deze keer hebben we informatie gekregen over het "**dit ben ik profiel**". Hij heeft verteld wat er zoal in het **dit ben ik profiel** moet komen te staan. Ook hebben we bepaalde punten met de klas bepaald.  
**De inhoudelijke eisen:**
- communicatief sterk
- je interpretatie vermogen
- stukje over jezelf
- wat je bereikt hebt
- reeds geleverde prestaties
- verbeterpunten 
- leerdoelen 
- competenties

**Visueel/Vorm**
- recent gemaakte visuals
- linken als dat nodig is
- je eigen stijl
- geordend geheel
- chaotisch 
- profielfoto

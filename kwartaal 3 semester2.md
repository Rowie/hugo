---
Title: Semester 2, Kwartaal 3
Date: 2019-04-03
---

*14-02-2019*
Om te beginnen heb ik vandaag een google-drive map aangemaakt zodat ieder teamlid zijn werk kan delen met ons. 
De opdracht was om een explainer te maken over AI, om daar mee te beginnen heb ik daar onderzoek naar gedaan en heb ik veel video’s gekeken om te leren wat een explainer precies inhoud. 

*15-02-2019*
Samen met Lotte en Demi hebben we de explainer afgemaakt zodat deze kon worden gepresenteerd aan de studenten.

*19-02-2019*
Ik wilde de rol als visual designer op me nemen omdat ik hier meer van wil leren, een extra uitdaging met 3 grafisch designers in mijn team. Om te beginnen heb ik een styleguide gemaakt van onze huisstijl. Dit is uitgelopen op het ontwerpen van de stijl van onze verslagen en producten. Lotte heeft mij laten zien hoe ik dat het beste kan aanpakken dus we hebben het vooral samen gedaan. 

1e VDlab, uitleg gehad over wat VD is, vormgeven van een communicatie doel op een functionele en esthetisch aangename manier. -> afbeeldingen vergeleken, logo ontwerp, visuele merkanalyse+concurrentie

Eerste les Indesign gevolgd.

*22-02-2019*
Tijdens lab heb ik visueel veldonderzoek gedaan. Dit heb ik gedaan door een doel op te stellen voor ik naar Witte de With ging. Mijn doel was om erachter te komen wanneer, waar en hoe men het meest nieuws consumeert. Ik moest gaan kijken naar een patroon. Helaas waren er niet veel mensen in de ochtend dus heb ik gekeken naar patronen op Witte de With.

Deze ochtend hebben we een briefing gehad van NRC, hier werd veel nuttige informatie verteld bijvoorbeeld over de doelgroep. 

Tijdens studio hebben we ons gefocust op een plan opstellen en onderzoek, ook heb ik mijn visuele merkanalyse afgemaakt. 

*05-03-2019*
Vandaag hebben we een kleine presentatie gegeven over ons team, hierin werd verteld wat de afspraken waren en onze doelen. Achteraf hebben we een teamdocument opgesteld met alle details erin.

Tijdens lab heb ik geleerd over datavisualisatie en moest ik onderzoek gaan doen naar mijn onderwerp voor de infographic.

*08-03-2019*
Tijdens lab ben ik aan het werk gegaan met de infographic. Hiervoor heb ik eerst een personal journey gemaakt over mijn ochtendroutine en heb ik newspapers ingevuld met kleuren om te vergelijken wat verschillende kranten doen. Voor ik de infographic kon maken heb ik onderzoek gedaan en dat uitgetekend, het gaat over de doelgroep van NRC. Door dat ik deze data al heb gevisualiseerd kan ik het gaan uitwerken met InDesign, dan is het klaar voor het verslag. 

Ik heb tijdens studio verder gewerkt aan de infographic en heb besloten om met Lotte een concurrentie analyse te maken. Om deze te maken hebben we gekeken naar AD, NOS, NU en NRC. We hebben gekeken naar aspecten op de website en de app. 


*12-03-2019*
Vandaag heb ik een gesprek gehad met Ellen over mijn doelen en wat ik moet doen om mijn herkansing te halen. Om het duidelijk te krijgen heb ik een wrap up gemaakt over dit gesprek met daarin per competentie mijn doelen. Deze heb ik gemaild naar Ellen zodat zij ook een overzicht heeft.

Lotte en ik hebben de concurrentie analyse afgemaakt. Met het team hebben we overleg gehad over onderzoekmethodes, Demi kwam met een foto-onderzoek. Wij hebben allemaal een paar mensen gevraagd om deel te nemen aan dit onderzoek. Verder heb ik deskresearch verricht naar NRC/doelgroep

Tijdens lab ben ik begonnen aan een poster ontwerp, dit heb ik gedaan door verschillende lay-outs te tekenen. Ik heb er uiteindelijk een uitgewerkt zodat er een template was voor de onderzoeksposter.

*15-03-2019*
Vandaag is mijn poster ontwerp aangepast door Lotte en Jordan, zij hebben ervoor gezorgd dat deze al zo goed als af is. Ik heb mijn producten afgemaakt zodat deze klaar waren voor in de poster. 
Ik wilde alvast verder kijken op onderzoek naar de doelgroep dus heb ik vragen opgesteld voor interviews met krantenlezers.

*19-03-2019*
Ik heb al een interview afgelegd met een krantenlezer, Demi heeft ook een aantal interviews. Vanuit deze interviews hebben we een persona samengesteld. We hebben gekeken naar opvallende dingen en wat we terug zagen komen. 
Verder heb ik mijn deel voor het verslag afgemaakt met stukjes tekst erbij en de interviews in een document gezet. 
Ik heb mijn styleguide afgemaakt zodat deze ook in het ontwerpverslag kon en het intro stuk voor het verslag geschreven.

S’avonds heb ik indesign gevolgd.

*20-03-2019*
Het was belangrijk om een kleine planning voor mezelf te maken zodat ik wist waar ik aan toe was. Zo raak ik niet in paniek of vergeet ik dingen. Ik ben begonnen met het kijken van video’s over presentatie technieken. Hier heb ik geleerd dat een open houding belangrijk is, bijv je armen langs je lichaam houden. Ook is je stemgeluid en manier van praten belangrijk zodat het niet saai wordt. Hiermee houd je de aandacht van het publiek bij je, ook kun je pauzes inlassen voordat je iets belangrijks gaat vertellen. Dit zorgt ervoor dat dat gedeelte ook extra aandacht krijgt. 
Ik heb opgeschreven waar producten voor dienen, wat ik eruit heb gehaald en wat de vervolgstap was/gaat zijn. Zo word ik me bewust van de producten.

*21-03-2019*
Vandaag heb ik de interviews verwerkt en inzichten geselecteerd, deze heb ik gedeeld met het team. Ik heb het ontwerpverslag mee afgemaakt, de poster geprint en de presentatie voorbereid met Demi.

Ik heb portfolio les gevolgd en stagevoorlichting.

*22-03-2019*
Posterpresentatie aan NRC. Verliep niet helemaal soepel maar goede feedback gekregen, zitten in de goede richting.

*26-03-2019*
Vandaag hebben we alle inzichten uit de interviews gehaald en de belangrijkste op een post-it geschreven. 
Ik heb deelgenomen aan de stand-up over de poster, ik moest vertellen waar onze poster voor stond en wat de feedback was van NRC.

Met lab ben ik naar het fotomuseum geweest, hier was een tentoonstellen over een opstand in Chili en de uitvaart van JF Kennedy.

Indesign gevolgd.

*28-03-2019*
Vandaag heb ik een gesprek gehad met mijn SC. We hebben besproken dat ik alles stap voor stap moet doen zodat ik niet weer terug val. Dit werkt voor mij rustiger en zit er minder druk op. Ik moet starrts schrijven en feedback vragen zodat ik weet hoe ik ze het beste kan schrijven. Ik moet een lijstje gaan maken met was mijn stage eisen zijn zodat ik een bedrijf kan zoeken.

*29-02-2019*
Lotte en Demi hebben een creatieve sessie voorbereid. Ik heb ideeën opgeschreven samen met mijn team. Ideeën voor nu en voor over 50 jaar. Deze hebben we bij elkaar gevoegd en daar zijn 4 concepten  uit gekomen. Sommige ideeën van mij zijn er ook in verwerkt.
We hebben feedback gevraagd, ook over het verslag.

*02-03-2019*
De taken zijn verdeeld, Demi en Lotte gaan het ontwerpverslag aanpassen en ik en Jordan gaan de concepten uitwerken. De concepten hebben we specifieker gemaakt door te kijken naar de inzichten en de ontwerp criteria. Nu deze zijn bedacht hebben we 3 conceptposters gemaakt en we hebben besloten dat de prototypes storyboards worden, daarvoor zijn de taken verdeeld ik moet er ook een maken.

*03-03-2019*

Ik heb vandaag een starrt geschreven zodat ik feedback kan vragen aan Ellen.
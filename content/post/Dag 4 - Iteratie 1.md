---
Title: Dag 4 - Iteratie 1
Date: 2017-11-22
---

Vandaag heb ik samen met een collega een interview opgesteld, met dit interview willen we naar Paard gaan om onze doelgroep te interviewen. Zo komen we te weten wat hun behoeftes zijn bij het uitgaan en wat zij belangrijk vinden.
Daarnaast heb ik de workshop D-Scrum gevolgd. Bij deze workshop werd er verteld hoe je de scrum methode toe past en welke mogelijke manieren er zijn. 
De competenties die ik wil behalen heb ik even op een rijtje gezet zodat ik weet waar ik aan moet voldoen en zo kan ik het gemakkelijker plannen voor mezelf. Zo raak ik niet in de knoei en het scheelt me een hoop stress.

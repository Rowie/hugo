---
Title: Dag 13 - Iteratie 1
Date: 2017-12-06
---
Vandaag hebben we een **pitch** over ons concept moeten houden voor *Fabrique* (onze opdrachtgever). Een pitch houd in dat je in 1 minuut je concept duidelijk moet maken. *Fabrique* heeft uiteindelijk feedback gegeven over wat er beter kan en wat ze graag willen, ook hebben we feedback gekregen van docenten. Verder hebben we nabesproken over wat we wel en niet gaan veranderen aan het concept.
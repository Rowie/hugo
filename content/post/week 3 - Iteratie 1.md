---
Title: Week 3
Date: 2018-04-19
---

23-04-2018
Vandaag ben ik samen met mijn team naar de Tarwewijk in Rotterdam Zuid gegaan om de interviews af te leggen. We zijn opgesplitst in tweetallen zodat we meerdere mensen konden benaderen. In het totaal hebben we 5 mensen kunnen interviewen en daar hebben we verschillende inzichten uit verkregen. Na afloop zijn wij terug gegaan naar school en hebben we de uitkomsten geanalyseerd en gedigitaliseerd. Daarnaast heb ik ook nog het interview naar bekende gestuurd die in de Tarwewijk wonen, deze uitkomsten kunnen ook van pas komen.

24-04-2018h
Er is vandaag een inspiratie lezing geweest over 'City as text', hier heb ik aan deelgenomen en er werd verteld hoe je op een andere manier de wijk kan waarnemen. Het was interessant want zo kan ik dit weer toepassen in het vervolg met mijn team.

25-05-2018
Ik wilde vandaag vooral werken aan een aantal deliverables zodat ik voor mezelf ook op schema blijf. Zo ben ik begonnen met een persona maken, dit doe ik door te kijken naar de inzichten die ik heb verkregen met de interviews. Daarnaast heb ik een styleguide gemaakt van onze huisstijl en deze kan ik bij de competentie uitwerken en verbeelden toevoegen. Ik heb meteen feedback gevraag aan onze alumni Robin en hij heeft me een aantal verbeter punten gegeven. Deze heb ik vastegelegd in een document en daarna heb ik de styleguide meteen aangepast. Nu moet de styleguide alleen nog gevalideerd worden.
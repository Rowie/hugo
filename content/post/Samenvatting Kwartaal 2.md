---
Title: Samenvatting kwartaal 2
Date: 2018-01-18
---
Dit kwartaal draaide alles om een interactieve oplossing te vinden voor Paard,
Den Haag. Het moest ervoor zorgen dat er meer herhaalbezoeken plaats
zouden vinden. De doelgroep was de nieuweling - 16 t/m 22 jaar.
Ik ben met mijn team gaan zoeken naar die oplossing en dat met hulp 
van onderzoeken.

De eerste iteratie draaide vooral om het onderzoek doen naar wat onze 
doelgroep belangrijk vond bij een avondje uit. Ik heb me dit keer weer 
gericht op het onderzoek ivm de feedback van vorig kwartaal. Aangezien 
het in het begin een beetje rommelig was met het team heb ik me helaas
niet zo verdiept als zou moeten, daar baal ik van want het was een leerdoel
voor me om beter te kunnen onderzoeken. Dit leerdoel blijft dus bij deze staan
voor de volgende periode.

Vanuit dat onderzoek ben ik visuele styleguides gaan maken over Paard 
zelf en de concurrentie, deze zijn ook te zien in mijn leerdossier. Vanuit 
die styleguide heb ik zelf een paar schermen ontworpen alleen zijn dat niet
de eindschermen geworden. In overleg met het team hebben we bepaalde
kleuren aangehouden voor ons concept. Ik vond deze kleuren ook het beste
passen bij ons concept omdat het vrolijke kleuren zijn en het is  een goede 
combinatie. De kleuren en de stijl zijn ook onderscheidend van de 
concurrentie van Paard.

Ik heb dit kwartaal wel weer een paar dingen geleerd zoals het maken van 
een testplan, een beetje beter worden in het terugkoppelen van onderzoek 
naar ontwerp, het maken van een flowchart (ik wist niet eens wat dit was). 
Ik heb ook weer visuele dingen gemaakt om daar wat beter in te worden zoals
posters en flyers, dit ook met behulp van Floris.

Daarnaast heb ik ondersteunend tekenen, Engels en Nederlands gevolgd,
tekenen heb ik gehaald! Ook ben ik begonnen met Photoshop alleen werd me
dit een beetje te veel dus heb ik dat opgegeven. 
Ik heb ook 3 verschillende workshops gevolgd en dat waren D-Scrum,
Letterontwerp en HTML-CSS. Dat was erg fijn om een keer gehad te hebben
want dan weet ik daar nu ook meer van, en wie weet kan ik het nog gebruiken 
in de toekomst.

Nu op naar kwartaal 3! 
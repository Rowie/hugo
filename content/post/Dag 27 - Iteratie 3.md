---
Title: Dag 27 - Iteratie 3
Date: 2017-10-11
---

Vandaag hadden we weer studiodag, tenminste 's morgens. Hier hebben we vooral besproken wat we nog konden veranderen enz aan de game. We liepen steeds vast dus hebben dit even gelaten voor wat het is. Verder hebben we aan de deliverables gewerkt voor deze iteratie.

In de middag zijn we begonnen met een workshop over de *Starrts*, deze workshop werd gegeven door onze coach. Hij heeft ons *Starrts* laten schrijven, een 'beoordelingsgesprek' gevoerd met iemand om duidelijk te maken wat er precies aan bod komt. Daarnaast waren er ook peercoaches aanwezig die ervaring hebben met zo'n gesprek, hun konden ook erg veel informatie delen met ons.
We hebben hier erg veel van geleerd. 
Een starrt houd het volgende in:
Een starrt is een methode om een gesprek te voeren. Eerst vul je dit in op een formulier zodat je al goed voorbereid bent op het eindegesprek. In een starrt beschrijf je de volgende punten; **S**ituatie, **T**aak, **A**ctie, **R**esultaat, **R**eflectie en **T**ransfer. Zo kan je op een duidelijke manier jouw competenties toelichten aan de beoordelaars.
Na deze workshop had ik ook nog coachuur, hier heeft de coach met onze klas nog teruggeblikt op de workshop. Er zijn vragen gesteld en ook is er besproken waar behoefte aan was. Nou zijn we tot de conclusie gekomen dat we allemaal behoefte hadden aan nog een extra les over het leerdossier. Deze word ingepland op de dinsdag na de herfstvakantie. Als huiswerk moeten we al een aantal starrts schrijven om het onder de knie te krijgen. 

En nu, VAKANTIE!
---
Title: Dag 16 - Iteratie 2 
Date: 2017-12-12
---
Vandaag heb ik weer ondersteunend Nederlands gehad en daarnaast heb ik de workshop *Letterontwerp* gevolgd. Dit ging over verschillende soorten letters en wat de benamingen zijn, ook zijn er redelijk veel verschillende stijlen. Daarnaast hebben we geoefend om verschillende letters te tekenen/schrijven.
Ik heb besloten om te stoppen met tools for design - photoshop, dit heb ik gedaan omdat ik het niet ga redden om het bij te houden. Ik heb namelijk al 3 extra vakken erbij dus het werd mij iets te veel. Dit ga ik een andere keer overnieuw doen als ik wat minder extra vakken heb.
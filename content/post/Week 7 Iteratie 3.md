---
Title: Week 7
Date: 2018-06-01
---

28-05-2018
Vandaag zijn we begonnen met de laatste week, om deze meteen goed te starten hebben we het testplan afgemaakt. Ook hebben we vandaag feedback gevraagd op ons concept en het high-fid. Om nog wat te doen voor V&U heb ik een poster ontworpen voor de expo.

29-05-2018
Vandaag hebben ons high-fid getest in de tarwewijk. Na deze test hebben we meteen de resultaten en aanbevelingen verwerkt in de testrapportage zodat we dit woensdag in ons prototype kunnen aanpassen.

30-05-2018
Vandaag hebben we de app aangepast zodat deze helemaal klaar zou zijn voor de expo. Ook hebben we de testrapportage afgemaakt en heb ik een pitch opgesteld.

01-06-2018
Vandaag heb ik de eindexpo gehad, we hebben ons product gepresenteerd en de opdrachtgevers waren er erg enthousiast over. Dit voelde wel goed. Ook heb ik geprobeerd om te pitchen maar dit ging erg slecht. Ik had mezelf niet goed genoeg voorbereid en vond het ook erg eng. Daardoor vergeet ik vaak wat ik moet zeggen en hang ik vast in mijn verhaal.
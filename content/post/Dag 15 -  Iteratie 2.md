---
Title: Dag 15 - Iteratie 2
Date: 2017-12-11
---
Vandaag ben ik begonnen aan de aanpassingen van mijn ontwerpcriteria, dit op basis van de feedback van de afgelopen validatie op 4 december. Het nieuwe validatie moment zou vandaag zijn maar is verplaatst naar volgende week aangezien we sneeuwvrij hebben gekregen! Verder heb ik vandaag nog wat dingentjes afgewerkt voor Nederlands en Engels.
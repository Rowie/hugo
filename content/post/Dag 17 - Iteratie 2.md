---
Title: Dag 17 - Iteratie 2
Date: 2017-09-26
---

Deze dag heb ik het laatste hoorcollege gevolgd van dit kwartaal. Dit hoorcollege ging over onderzoeken. Er werd verteld hoe je onderzoek kan gebruiken van anderen, hoe je zelf onderzoekt en over je onderzoekende houding. Ook werd er uitleg gegeven over wat geen onderzoek is.
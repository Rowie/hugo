---
Title: Dag 23 - Iteratie 2
Date: 2017-10-04
---

Vandaag heb ik mijn "Dit ben ik profiel" afgemaakt en ingeleverd, hier staat namelijk een deadline voor vast.  

Ook hadden ik met mijn team een spannende presentatie over onze game, deze moesten we houden voor leraren en de **alumni**. **Alumni** zijn mensen van buitenaf die deze studie gevolgd hebben en al in het bedrijfsleven zitten, zij kunnen misschien op een andere manier naar een game kijken dan de leraren dus dat is fijn voor feedback.

Bij de presentatie hebben 2 teamgenoten het woord gedaan om het duidelijk te houden. 1 observeerde, 1 schreef feedback en 1 filmde het.
Ik heb geobserveerd en heb dus ook goed naar de feedback geluisterd, en ik heb gezien hoe de leerlingen reageerden op de game. 
Als feedback op de game hadden we dat het concept in goede volgorde werd gepresenteerd. Daarnaast hebben we ook veel punten mee gekregen waar we nog meer onderzoek naar konden doen, bijvoorbeeld: de kennis van als iets niet werkt, waarom audio en geen video/foto's, beter onderzoek doen naar de doelgroep wat ze nou echt leuk vinden en tot slot wat we in de wachttijden van de game zouden kunnen doen.
Daar zijn we natuurlijk mee aan de gang gegaan maar daar kom ik later op terug! 

Aan deze feedback hadden we heel veel en we kunnen het allemaal meenemen naar de volgende iteratie en kwartaal zodat we meteen sterker beginnen!

Later op deze dag hebben we nog les gehad van onze coach, daar heb ik weer erg veel uitleg gehad over de *Starrts*. Hier kom ik ook later op terug als ik er meer over weet.

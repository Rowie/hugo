---
Title: Week 1
Date: 2018-04-12
---

09-04-2018
Vandaag zijn we begonnen met de kick-off van deel 2 van semester 3 (Kwartaal 4). Er is weer even opgefrist waar de opdrachtgever nou eigenlijk naar op zoek is. Het is vanaf nu de bedoeling dat er ook echt onderzoek wordt gedaan in de wijken van Rotterdam Zuid. Hiervoor moeten we een bepaalde wijk kiezen waar we op focussen. 
Er moest weer een nieuwe voorzitter komen en we hebben er voor gekozen dat ik die taak op me neem. Op deze manier leer ik om door te groeien en mezelf te verbeteren, aangezien ik liever op de achtergrond blijf. Aangezien ik deze taken op me heb genomen moet ik natuurlijk het team proberen te motiveren op mijn eigen manier en dat moet zijn weg nog een beetje gaan vinden. Ik heb daarvoor wel teamafspraken opgesteld zodat het duidelijk is wat iedereen van elkaar verwacht. Vandaag heeft iedereen ook bepaald welke beroepsproducten zij willen gaan halen.

11-04-2018
Nadat maandag is bekeken welke beroepsproducten mijn teamgenoten willen behalen, heb ik samen met Lotte een invertarisatie kunnen maken. Hier in staat duidelijk wie wat wil doen. Verder hebben we deze week rustig aan gedaan en de Tarwewijk een beetje onderzocht.

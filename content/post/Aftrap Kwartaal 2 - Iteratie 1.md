---
Title: Aftrap Kwartaal 2 - Iteratie 1
Date: 2017-11-14
---

Vandaag beginnen we aan kwartaal 2. Voor de briefing over de opdracht moeten we naar Paard in Den Haag. Dit is een Evenementcentre en café. De opdracht die we hebben gekregen is opzich erg leuk maar ook wel pittig. We moeten met ons projectgroepje een oplossing bedenken in vorm van een app/website waardoor er vaker herhaalbezoeken plaatsvinden. Dit is erg weinig dus aan ons de taak om dat te veranderen! Ook hebben we een film gekeken over *Design Thinking* dit was ook erg interessant, in de film werd uitgelegd wat dat in houd. 
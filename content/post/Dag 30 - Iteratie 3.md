---
Title: Dag 30 - Iteratie 3
Date: 2017-10-25
---

Ja hoor, vandaag hebben we tot slot de expo gehad. Het ging erg goed, we hebben veel positieve feedback gehad maar ook verbeterpunten. Dit kunnen we allemaal weer meenemen naar het volgende kwartaal! Veel mensen waren enthousiast over de game, dat was erg fijn om te horen.
Verder heb ik vandaag peerfeedback ingevuld voor mijn teamgenoten aangezien dat ook in het leerdossier moet komen te staan. En later deze middag hebben we de laatste les van onze coach gehad over het leerdossier.
In deze les is een leerdossier laten zien zodat we een voorbeeld hebben en makkelijker vooruit kunnen. Ook zijn de laatste obstakels besproken en vragen gesteld. Dit was erg fijn want nu weten we duidelijk wat er in het leerdossier moet en hoe we het moeten verwerken!

Het was weer een drukke dag met veel informatie, en morgen nog een dag knallen om mijn leerdossier af te krijgen voor de deadline! *Wish me luck*
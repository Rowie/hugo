---
Title: Dag 7 - Iteratie 1 
Date: 2017-11-27
---

Weer een nieuwe projectdag die vooral bestond uit het maken van een concurrentie onderzoek. Daarbij heb ik dus onderzocht wat eventuele concurrenten zijn van Paard en waarom. Ook heb ik meteen gekeken naar de huisstijl van deze clubs/café's en daar een styleguide van gemaakt. Dat heb ik gedaan zodat we makkelijk onderscheid kunnen maken van de concurrenten. 
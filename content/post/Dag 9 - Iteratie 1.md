---
Title: Dag 9 - Iteratie 1
Date: 2017-11-29
---

Nog een dag ziek in mijn bed, toch heb ik vandaag een begin gemaakt aan de styleguid van Paard. Dit heb ik gedaan door van elke pagina de kleuren te gebruiken en afbeeldingen van de stijl.
Ik ben ook begonnen aan de ontwerpcriteria en heb deze uitgeschreven, en ik heb een beginschermpje gemaakt met de stijl van Paard.
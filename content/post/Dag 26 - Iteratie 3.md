---
Title: Dag 26 - Iteratie 3
Date: 2017-10-09
---

Start van iteratie 3! Dit betekent dus ook de laatste iteratie van het 1e kwartaal. Vandaag is duidelijk geworden dat deze iteratie 1,5e week duurt, dat zonder de herfstvakantie meegerekend, erg kort dus. We moeten deze periode aan de gang met de feedback die we hebben gekregen bij de presentaties voor *Alumni*. Dat zijn we dus ook gaan doen. 

We zijn begonnen met een aantal dingen bespreken, zoals wat we moesten toevoegen aan de game en wat we moesten veranderen. Ook moesten we wat meer onderzoek doen en dat hebben we gedaan. Deze dag hebben we vooral besteed aan dingen die we individueel nog moesten bij werken, in mijn geval mn blog.
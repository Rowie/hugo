---
Title: Week 4
Date: 2018-05-09
---

07-05-2018
Vandaag ben ik begonnen met het maken van een testrapportage zodat ik deze kon laten valideren. Dit heb ik vandaag ook laten doen en hij is voldoende mits ik er nog hypotheses in zou verwerken. 

08-05-2018
Vandaag ben ik weer naar een hoorcollege geweest.

09-05-2017
Vandaag heb ik opnieuw de workshop empathie aangevraagd, eindelijk wordt deze ingepland. We hebben twee creatieve technieken toegepast op ons concept, een over hoe dat het concept over 5, 10, 20, 50 en 100 jaar zou zijn. En een door de gekste ideeën op te schrijven. Ook heb ik met Bob gepraat over welke creatieve technieken ik het best toe zou kunnen passen. Na de creatieve technieken hebben we gewerkt aan de insight cards.
Ook heb ik vandaag de herkansing Engels gemaakt en de herkansing van het Nederlands portfolio ingeleverd.
---
Title: Dag 20 - Iteratie 2
Date: 2017-09-29
---
Vandaag heb ik een 0-meting tekenen gehad. Hier mochten we kiezen tussen een realistische hand zonder te gummen of een perspectief tekening zonder liniaal te gebruiken. Ik had gekozen voor de realistische hand. Ook moesten we een storybord tekenen van maximaal 6 afbeeldingen. Dit moest een storybord zijn over de reis van huis naar school en er moest emotie in afgebeeld worden. Fingers crossed dat het is goed gegaan! 
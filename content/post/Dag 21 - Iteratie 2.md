---
Title: Dag 21 - Iteratie 2
Date: 2017-10-02
---

Vandaag zijn we begonnen met het spel "**black stories**", dat doen we elke dag en is ons ritueel geworden om te starten. s'Morgens hebben we een compleet nieuw concept bedacht voor de game, het duurde even maar we zijn tot een besluit gekomen. 

De game luidt als volgt; Beide teams hebben hun eigen startpunt. Vanuit dat startpunk zal je via verschillende "tussenlocaties" het eindpunt moeten bereiken. Hoe eerder je dit eindpunt bereikt, hoe meer punten je verdient. Wees dus sneller dan het andere team. 
Kies ervoor om het andere team tegen te werken of help ze juist. Zij hebben jouw hints voor jouw "tussenlocaties" en jij die van hen. Communiceer met elkaar en voer de opdrachten op de kaarten uit. De kaarten vind je bij de **peercoaches** die op de tussenlocaties staan.

Vervolgens zijn we begonnen met het prototype van de game en hebben we een naam moeten verzinnen, maar daar hebben we een oplossing voor bedacht! We hebben verschillende woorden opgeschreven die we bij de game vonden passen, van die woorden hebben we propjes gemaakt en daar hebben we uit gegrabbeld. De woorden die eruit kwamen waren **unity en team**, het grappige hiervan is dat de woorden een soortgelijke betekenis hebben. We hebben ze samengevoegd en de game heet nu **uniteam**.

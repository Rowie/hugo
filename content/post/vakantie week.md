---
Title: Meivakantie
Date: 2018-05-05
---

Deze week was het tijd voor een beetje rust. Naast dat ik veel heb gewerkt heb ik ook nog tijd besteed aan school. Zo heb ik mijn blog bijgewerkt, mijn portfolio voor Nederlands verbeterd ivm herkansing. Ook heb ik mijn persona afgemaakt en ben ik begonnen met schrijven van mijn STARRT's zodat ik niet achter ga lopen. Dit scheelt voor mij wat stress.
---
Title: Terugblik op het 1e kwartaal
Date: 2017-10-26
---

In het begin van dit kwartaal had ik nog totaal geen idee wat er op me af zou komen, dus liet ik het allemaal maar over me heen komen. Het was toch wel even een grote verandering om van fashion&design over te gaan naar een studie multimedia design. 
Daarnaast kende ik de personen nauwelijks waar ik bij in het groepje zat ondanks dat we op het intro kamp al wel een beetje hadden gepraat. 

**Iteratie 1**

In de eerste iteratie kregen we als team de opdracht om een game te ontwerpen voor de eerstejaars studenten van 2018, deze game moest de studenten kennis laten maken met de stad Rotterdam. We hebben voor deze opdracht de deliverables verdeeld over de teamleden. Ik kreeg de taak om onderzoek te doen naar de doelgroep, de doelgroep waar we onze game voor wilde ontwerpen waren de architectuur studenten van WDK.   Voor het onderzoek heb ik een enquête gemaakt en deze op een facebookpagina geplaatst. Uiteindelijk kwamen we erachter dat we telkens een beetje vast liepen door de doelgroep waardoor we zijn geswitched naar CMD'ers als doelgroep. Dit was even een omschakeling waar ik meestal veel moeite mee heb omdat ik dan alles aan de kant moet zetten maar ik kwam hier opzich best snel doorheen! Vervolgens heb ik hier weer deskresearch over  gedaan, heb een nieuwe enquête gemaakt en ben ik het in mijn dummy gaan opschrijven. Ook ben ik afbeeldingen gaan zoeken en screenshots/foto's gaan maken van mijn onderrzoek aangezien ik het visueel moest maken.
Dit was al meteen een uitdaging voor mij omdat ik best wat moeite heb met het onderzoek doen naar dingen. Op deze manier train ik dat meteen goed. Ook heb ik een moodboard moeten maken over de doelgroep, hier had ik wat minder moeite mee omdat ik al erg bekend ben op het gebied van moodboards. Dat komt door mijn vorige studies. Ook moest er een 'dit ben ik profiel' worden ingeleverd dus dat heb ik uiteraard ook gemaakt.
Tijdens de 1e iteratie hebben we een super leuke game bedacht en uitgewerkt waar we allemaal erg enthousiast over waren. 

Het concept in het kort uitgelegd:

In de game moeten de nieuwe studenten peercoaches gaan zoeken met behulp van kledingstukken die ze aan hebben. Elk groepje begint met 10 punten, met die 10 punten kunnen ze bijvoorbeeld een kledingstuk unlocken. Hoe meer punten hoe meer kledingstukken ze kunnen unlocken. Per locatie word er door de game afgesteld welk kledingstuk gedragen word op die locatie, zodat de goede kleding geshowd word. Elke keer als ze een peercoach hebben gevonden krijgen ze punten, hoe sneller ze zijn hoe meer punten het opleverd. Ook worden ze door de game dan naar een andere locatie gestuurd. Door de kledingstukken kunnen ze een peercoach makkelijker vinden. Het spannende aan de game is dat ze misschien helemaal de verkeerde mensen aanspreken omdat die toevallig het zelfde aanhebben als de peercoach.

We hebben deze game dan ook gepresenteerd aan het eind van de iteratie, en we kregen erg goede feedback. Ook kregen we te natuurlijk verbeterpunten te horen maar daar kunnen we gelukkig op terugpakken in de volgende iteratie.


**Iteratie 2**

In iteratie 2 kregen we de opdracht *"kill your darling"**, dit betekende dat we onze game helemaal overboord moesten gooien. Dit was behoorlijk klote vooral omdat ons concept zo goed was, en dat wilde we graag verder uitwerken. 
We zijn meer onderzoek gaan doen naar verschillende games, de doelgroep en we moesten deze keer ook creatieve technieken toepassen. Daarom ben ik met sommige teamgenoten de workshop **creatieve technieken** gaan volgen. Daar hebben we gewerkt met de *Rood en Groen licht fase. Bij die methode gebruik je een vel met 4 vakken erop, in die vakken staan de woorden *realiseerbaar, onrealiseerbaar, realiseerbaar orgineel en onrealiseerbaar origineel*. We moesten gaan brainstormen op een onderwerp, daar dus oplossingen voor bedenken. Die oplossingen moesten we verdelen over die vlakken en uit eindelijk steeds meer selecteren tot je uiteindelijk bij het beste idee komt. Dit is dus een hele fijne manier om te werken, vind ik zelf. 
De taken die ik kreeg waren; De planning maken, moodboard verbeteren na beter onderzoek, en de ontwerpproceskaart bijhouden. Natuurlijk heb ik ook weer veel mee gewerkt aan het nieuwe concept en de uitwerking daarvan. Het duurde wel even voordat ik en mn team een game hadden bedacht waar we allemaal 'tevreden' over waren.

**1e Concept van de game:**

Een deel van het team blijft binnen en een ander deel gaat Rotterdam in.  Het team binnen geeft het andere team aanwijzingen waar ze heen moeten door te bellen. Elke keer als ze bij een locatie zijn gekomen krijgen ze een gekke opdracht waardoor ze bijvoorbeeld in eens iets moeten schreeuwen op een drukke plek of een piramide van zo veel mogelijk mensen moeten bouwen. Als deze opdracht was voldaan zouden ze weer naar de andere locatie worden geleid door het andere team dat binnen zit. Zo zou het spel lopen tot de laatste opdracht en daarna zou het snelste team winnen. Er zat dus ook tijd druk achter.

Over de game zijn we feedback gaan vragen aan een projectleider. Hij kwam nog met heel veel punten waar niet echt veel over nagedacht waren en wat er zoal verbeterd kon worden. Daarmee konden we dus zeker verder en hebben gebaseerd op die feedback een redelijk goede game kunnen ontwerpen. Die feedback hebben we ook vastgelegd. 

Toen we eenmaal onze game zo goed als af hadden liepen we toch nog tegen een punt aan, er was geen goede oplossing voor de wachttijden in de game. Ik en mn team moesten dus flink gaan onderzoeken wat voor eventuele minigames of opdrachten die wachttijd zou kunnen opvullen. Ik heb er heel veel over nagedacht en ben ook met best goede ideeën gekomen, maar voegde het wel iets toe? Uiteindelijk hebben we besloten om er nog niets bij te voegen en te wachten op de feedback van de *Alumni*.  

**2e Concept van de game:** 

 Beide teams hebben hun eigen startpunt. Vanuit dat startpunk zal je via verschillende "tussenlocaties" het eindpunt moeten bereiken. Hoe eerder je dit eindpunt bereikt, hoe meer punten je verdient. Wees dus sneller dan het andere team. 
Kies ervoor om het andere team tegen te werken of help ze juist. Zij hebben jouw hints voor jouw "tussenlocaties" en jij die van hen. Communiceer met elkaar en voer de opdrachten op de kaarten uit. De kaarten vind je bij de **peercoaches** die op de tussenlocaties staan.

Ik en mijn team hebben ons klaar gemaakt voor de presentatie die gegeven werd voor leraren, *de Alumni* en leerlingen. Er is op die presentatie redelijk goede feedback gegeven maar ook verbeterpunten werden benoemd waar we later mee aan de slag konden. Zo moesten we inderdaad gaan kijken wat de oplossing kon zijn voor de wachttijden. Ook was de vraag, 'Waarom alleen audio en geen video/afbeeldingen om te communiceren?'. Daarnaast moest er ook meer onderzoek gedaan worden naar wat de doelgroep nou precies wil en interessant vind. 

**Iteratie 3**

In iteratie 3 moest de feedback verwerkt worden en moesten de puntjes op de i gezet worden. De game moest af zijn en er zat een expo aan te komen waar overnagedacht moest worden. 
Ik en m'n team zijn eerst aan de slag gegaan met individuele opdrachten die nog afgemaakt moesten worden enzovoort. Daarna zijn we gaan kijken hoe we de game konden verbeteren en hebben het dus ook veranderd. Ik heb ook een week vakantie gehad waar ik aan de **Starrts** moest werken voor het leerdossier en ik moest voor na de vakantie 2 oplossingen uitgewerkt hebben voor de wachttijden in de game. Ook moest ik wat dingen verzinnen die interessant zouden zijn voor de expo en hoe we de game gingen presenteren. Dit heb ik netjes gedaan zodat we na de vakantie doorkunnen.

Teruggekomen van de vakantie zijn we aan de slag gegaan over de game, ik heb mijn ideeën in de groep gegooid en mijn teamgenoten hun ideeën. We kwamen er alsnog niet uit omdat niks echt heel erg toevoegde aan de game dus besloten ik en m'n team om de wachttijden te laten en ongevraagde feedback neer te leggen bij de expo.

**3e Concept van de game:**

In Uniteam strijdt je met en tegen elkaar. Elk team heeft zijn eigen startpunt, maar alle teams hebben hetzelfde eindpunt. De vraag is wie hier als eerste aankomt. Iedereen is verdeeld in groepen van twee teams. Elk team heeft zijn eigen route naar het eindpunt. Op deze route liggen allemaal tussenlocaties waarvoor hints zijn te vinden. Als groep help je elkaar verder door deze hints door te geven. Het ene team heeft de hint voor de tussenlocatie van het andere team in de groep en het andere team heeft de hint voor jouw team.  Je kan alle tussenlocaties af gaan, maar je kan ook iets anders doen. Om de vijf minuten krijgt iedereen een hint naar de eindlocatie. Denk je te weten waar deze ligt, rennen. Het andere team in de groep loopt vast, omdat ze van jou geen hint meer krijgen naar de volgende tussenlocaties en staan dus buitenspel. Jij komt eerder bij het eindpunt aan en wint de game. Dus de vraag is, haal jij het eindpunt?

Voor de expo moesten we nog wat verzinnen om te presenteren, we hebben besloten om een leuk promo filmpje te maken in Rotterdam terwijl wij de game spelen. Het resultaat is erg leuk geworden!
We hebben de game gepresenteerd en hebben weer feedback gekregen. Alles draaide om feedback dit kwartaal en dat zal denk ik wel zo blijven, en alle feedback ga ik gebruiken in het volgende kwartaal om meteen sterker te starten. 

Ik heb de laatste dag nog een coachles gehad met heel veel informatie over het leerdossier aangezien daar ook nog een deadline voor is morgen. Dat word vandaag dus nog flink bikkelen.

Dit is mijn terugblik/samenvatting op het eerste kwartaal. Ik heb er voor mijn idee al aardig wat geleerd en ben ook opdrachten aangegaan waar ik meestal moeite mee heb. Daar heb ik me proberen overheen te zetten en met resultaat!
Op naar kwartaal 2!


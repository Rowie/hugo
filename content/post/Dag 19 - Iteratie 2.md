---
Title: Dag 19 - Iteratie 2
Date: 2017-09-28
---

Vandaag hebben we vele concepten bedacht voor een game. We zijn tot een concept gekomen waar er een deel van het team binnen blijft en er een ander deel Rotterdam in gaat.  Het team binnen geeft het andere team aanwijzingen waar ze heen moeten door te bellen. Elke keer als ze bij een locatie zijn gekomen krijgen ze een gekke opdracht waardoor ze bijvoorbeeld in eens iets moeten schreeuwen op een drukke plek of een piramide van zo veel mogelijk mensen moeten bouwen. Als deze opdracht was voldaan zouden ze weer naar de andere locatie worden geleid door het andere team dat binnen zit. Zo zou het spel lopen tot de laatste opdracht en daarna zou het snelste team winnen. Er zat dus ook tijd druk achter. Wij hebben feedback gevraagd aan een project leider. Hij zei dat er veel dingen anders zouden kunnen en veel tips hoe het dan anders zou zijn. Dit hebben we een beetje besproken en zijn tot een deel van het aangepaste concept gekomen. 

 Later in de middag hebben we een werkcollege gehad, dit was een vervolg op de hoorcollege eerder deze week. Hier hebben we  legoblokjes moeten sorteren in verschillende subcatogorieën bijvoorbeeld vorm, grootte, kleur enzovoort. Dit hebben we ook met verschillende afbeeldingen gedaan; kleur/zwart-wit, nep/echt, beelden/personen.

---
Title: Dag 24 - Iteratie 2
Date: 2017-10-05
---

Vandaag stond om half 2 een 0-meting NL/EN op de planning. Deze heb ik niet kunnen halen door een stilstaande trein waardoor ik natuurlijk heel erg baalde! Gelukkig heb ik de 0-meting in het inhaal moment kunnen maken om 15.15 uur. Daardoor mistte ik helaas wel het werkcollege over onderzoeken. Het scheelde dat meer dan de helft van de klas daar niet was en de les dus niet lang heeft geduurd. Heb ik niet heel veel gemist!
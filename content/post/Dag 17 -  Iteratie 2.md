---
Title: Dag 17 - Iteratie 2
Date: 2017-12-13
---
Deze dag heb ik redelijk wat gedaan aan het project. Zo ben ik begonnen met het maken van een ontwerpproceskaart, daarna heb ik mijn visuele styleguide aangepast. Ook heb ik mee gedacht aan het concept en het ontwerp, we zijn het er over eens dat we een strak ontwerp behouden en daarbij de kleurrijke kleuren van Paard, (Turquoise en Beige). Dit hebben we zo gelaten aangezien concurrenten veel donkere kleuren gebruiken, dus hier onderscheid Paard zich weer. Daarnaast hebben we ook besloten om de web-app niet te ingewikkeld te maken zodat het overzichtelijk en duidelijk blijft. Als iets ingewikkeld is hebben mensen namelijk niet veel zin om het uit te zoeken.
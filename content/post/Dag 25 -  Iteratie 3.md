---
Title: Dag 25 - Iteratie 3, Dag van de Expo
Date: 2018-01-17
---
Vandaag is een spannende dag geweest want we hadden onze eind expo. Hier kwam onze opdrachtgever van Fabrique kijken naar ons concept.
We hebben vandaag vooral alle laatste dingetjes afgemaakt en uitgeprint om te presenteren. Iedereen was erg druk in de klas omdat het de laatste dag was maar wel gezellig. Ook zijn we gezellig gaan eten met een aantal uit de klas. Na het eten zijn we terug gegaan naar school voor de Expo, deze begon om 18:30. We hebben onze pitch gehouden voor docenten en ons concept is uitgekomen op een voldoende! Ook hebben we voor Fabrique gepresenteerd en zij vonden het concept ook cool. 
Morgen nog een dag knallen voor het leerdossier en dan zit dit kwartaal er weer op!

---
Title: Semester 1, Kwartaal 2
Date: 2019-01-21
---

SPRINT 3.0 HERKANSING

**20-11-2018**
Sprint 3 is deze dag weer begonnen, de Design fase. Helaas hebben we een herkansing van sprint 1 en 2 dus zullen we daarmee de komende 3 weken moeten vullen.

**23-11-2018** 
We zijn begonnen met brainstormen hoe we de herkansing gingen aanpakken. Om meer contact te krijgen met de doelgroep voor onderzoek hebben we een flyer gemaakt met daarop een e-mail adres. Zo konden mensen contact opnemen met ons. Daarnaast hebben we probes samengesteld om daarmee informatie van de doelgroep te verzamelen richting ons concept. 

**27-11-2018**
Afgelopen weekend hebben we de probes in laten vullen door de doelgroep, de resultaten daarvan hebben we vandaag besproken. We hebben de uitkomsten vergeleken met elkaar. Om nog meer pogingen te doen tot contact met de doelgroep heb ik de huismeester een mail gestuurd met de vraag of hij ons eventueel kon helpen met het in contact komen met. Daarnaast heeft een teamgenoot contact gezocht met Joke  i.v.m een conflict tussen Woonstad en hun huurders. Dit kan eventueel belangrijke informatie zijn voor ons concept.

**30-11-2018**
Vandaag hebben we gevraagd om feedback op ons ontwerpverslag, we waren hier al aan begonnen om de herkansing te halen. Uit de feedback kwam dat het slim was om een visuele ontwerpproceskaart te maken, dat heb ik dan ook met Sanne gedaan. We hebben stap voor stap gekeken wat we hebben gedaan de afgelopen periode en dat erin verwerkt met tekst en schetsjes om te visualiseren.

**04-12-2018**
Ik ben vandaag begonnen met het opstellen van de ontwerpcriteria, het concept dat wij creëren moet voldoen aan deze criteria i.v.m de behoeftes van de gebruiker. Daarnaast hebben we veel gewerkt aan het verslag om deze op orde te krijgen voor de deadline aankomende donderdag.

**06-12-2018**
Deze dag draaide om alle puntjes op de i zetten van het verslag. Vandaag is de deadline en omdat we drie weken hard werken in de herkansing hebben gestopt en contact hebben proberen te verkrijgen met de doelgroep, hopen we zo dat we het halen! We hebben ook nagedacht over de presentatie aan Woonstad die morgen plaatsvindt, wat willen we weten en hoe pakken we het aan?

**07-12-2018**
De presentatie aan Woonstad heeft vandaag plaatsgevonden. We hebben ervoor gekozen om te vertellen in welke richting we aan het denken zijn. Daarnaast hebben we ervoor gekozen om ook de probes te laten invullen. Op deze manier konden we de resultaten van de doelgroep en van medewerkers van Woonstad naast elkaar leggen. Dit gaf voor ons een goed beeld van het verschil en konden hier mee aan de slag voor een verbeterd concept. We hebben ook veel feedback ontvangen met daarbij dingen waar we nog niet over hadden nagedacht. Zoals de privacy van de klanten en de realiseerbaarheid, ook andere opties zijn daarbij naar voren gekomen.

SPRINT 3.1 DESIGN

**11-12-2018**
We zijn vandaag begonnen met het bespreken van alle resultaten. Hier zit vaart achter aangezien we 3 dagen de tijd hebben om het design verslag van sprint 3 in te leveren. Nadat we alle resultaten hadden besproken zijn we gaan brainstormen om het concept aan te passen. Dit is uiteindelijk goed verlopen en er is een realiseerbaar concept uit voortgekomen. 

**13-12-2018**
Het design verslag moest vandaag worden ingeleverd dus hebben we daar veel aan gewerkt.  Ik heb de conceptbeschrijving uitgetypt en daarnaast was het een kwestie van alle informatie in het verslag plaatsen. Nu op naar de eindfase, deliver.

**14-12-2018**
Vandaag hebben we een low-fid prototype gemaakt, een beetje in het idee van wireframes. Zo kregen wij een duidelijker beeld en we zijn erachter gekomen waar we nog op vast lopen in het concept. 

SPRINT 4 DELIVER

**18-12-2018**
Het draaide vandaag om de prototype party, hierbij konden we bij andere teams feedback geven op de prototypes en dat konden ze ook bij ons. We hebben dit gedaan door een feedback formulier te maken en het concept uit te leggen. We hebben nuttige feedback gekregen waar we na de kerstvakantie mee aan de slag kunnen.

**08-01-2019**
Een frisse start na de vakantie heeft ons zeker goed gedaan. We hebben eens teruggekeken naar de feedback en ons concept weer aangepast. Het concept staat nu vast en we zijn er allemaal blij mee. Ik ben meteen begonnen met een testplan en mijn andere teamgenoten zijn begonnen aan het high-fid prototype. Vrijdag krijgen we de kans om het te laten testen door docenten. 

**10-01-2019**
Vandaag zijn we voor de locatie van Woonstad West Blaak gaan postten om ons prototype te testen, helaas zonder resultaat. Laurens kwam met het idee om bij zijn studentenvereniging onder de huurders te testen. Dit was een goede oplossing voor feedback op ons concept en op het prototype. We hebben nog besproken hoe we de expo willen gebruiken om ons concept te laten leven.

**11-01-2018**
Vandaag hebben we een test uitgevoerd bij de docent Rolf, we hebben goede feedback gekregen. Deze feedback hebben we meteen meegenomen en zijn gaan nadenken hoe we dingen konden toevoegen of eventueel aanpassen. Daarnaast ben ik begonnen aan de testrapportage en de rest van mijn team aan het high-fid.

**15-01-2019**
Vandaag hebben we erg veel gedaan, ik heb samen met Sanne de conceptposter, een flyer, en een poster gemaakt voor de expo. Daarnaast heb ik de testrapportage afgemaakt en resultaten besproken met mijn team, wat we zoal nog aan konden passen aan het prototype. 

**17-01-2019**
We maken vandaag ons laatste deel van het ontwerpverslag af aangezien de deadline is. Daarnaast printen we alvast wat spullen voor de proefexpo.

**18-01-2019**
De proefexpo heeft vandaag plaatsgevonden. Dit was een goede start en we weten nu wat beter moet met de eind expo waar ons werk daadwerkelijk beoordeeld wordt. We hebben vandaag ook het prototype getest en kunnen nog wat aanpassingen verrichten aan het prototype.

---
Title: Samenvatting Semester 1 Kwartaal 2
Date: 2019-01-21
---


Dit kwartaal heb ik geleerd dat er op veel meer manieren onderzoek kan worden gedaan, zo had ik bijvoorbeeld nog nooit gehoord over probes. Door deze manier in te zetten hebben ik veel informatie verkregen over de behoeftes van de doelgroep. Deze inzichten hebben ons tot een nieuw concept gebracht. Het concept leek het uiteindelijk toch niet helemaal te zijn na een aantal testen en feedback. Door veel te brainstormen en resultaten te bekijken zijn we bij een mooi eind concept gekomen. Om dit concept goed te presenteren hebben we een high-fid prototype ontworpen en heb ik samen met Sanne een conceptposter, een reclameposter en een flyer ontworpen. Daarbij kwam ook nog de inrichting van de expo kijken. 
Ik heb geleerd om op een visuele manier de ontwerpproceskaart te maken, wat ik uiteindelijk veel makkelijker vind dan digitaal. Daarnaast heb ik de ontwerpcriteria opgesteld en voor het eindconcept weer aangepast zodat er duidelijkheid zou zijn over de belangrijkste punten. 
Voor de eerste keer heb ik wat uitgebreidere conceptbeschrijvingen gemaakt waarbij alles meteen duidelijk is voor de lezer. Het kostte iets meer tijd maar dan heb je ook wat. 
Voor de test heb ik gezorgd dat er een testplan was zodat iedereen een duidelijke taak had en wat we er mee wilde bereiken. Welke vragen wilden we stellen en hoe pakte we de test aan? Uiteindelijk is dit iets anders gelopen dan verwacht omdat er op locatie niet veel klanten van woonstad waren. Dit hebben we opgelost door te testen bij een studentenvereniging en bij een docent. Vanuit deze tests zijn er natuurlijk resultaten gekomen en deze heb ik weer verwerkt in een testrapportage en zijn er aanpassingen verwerkt in het prototype.  
Begin dit semester ging alles heel erg moeizaam en daar ben ik langzaam weer bovenop gekomen mede door professionele hulp. Ik heb dit kwartaal dus weer een aantal dingen bijgeleerd door mijn teamgenoten. 

In kwartaal 3 sprint 3 wilde ik het LAB prototypen volgen, dit heb ik uiteindelijk niet gedaan omdat wij een herkansing hadden en de lesstof ging door naar de volgende fase. Ik was van mening dat ik op dat moment niet veel aan prototypen zou hebben  zolang er nog geen vast concept zou zijn. 
Sprint 4 wilde ik LAB visuals volgen, dit heb ik 3 lessen gedaan. Ik mistte de basis waardoor ik achterliep en veel dingen niet wist. Ik wil dit dus volgend semester op pakken, of een ander LAB waarbij ik met de basis begin en dus ook echt gestructureerd dingen leer. Op de afgelopen manier wordt ik veel te warrig in mijn hoofd en ik ben al redelijk chaotisch, daarom denk ik dat dit op dit moment een goede keus voor me is geweest.

Over het algemeen was het een leerzaam semester maar vond ik het ook een beetje te lang om met dezelfde opdrachtgever te werken.

Op naar de volgende.

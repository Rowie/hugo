---
Title: Dag 12 - Iteratie 1
Date: 2017-12-05
---
Vandaag heb ik een hoorcollege gehad over de sociale beïnvloeding van de mens. Dit gaat over hoe mensen denken *'automatisch of gecontroleerd*'. Ze praten over **sociale perceptie** en **sociale cognitie**, **systeem 1** en **systeem 2**, **heuristieken** en **schema's**. Dat zijn allemaal bepaalde methoden van hoe de mens denkt, interessant maar ook ingewikkeld. Na het hoorcollege heb ik weer ondersteunend Nederlands gevolgd.
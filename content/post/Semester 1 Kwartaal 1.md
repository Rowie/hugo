---
Title: Semester 1, Kwartaal 1
Date: 2018-10-21
---

**SPRINT 1**
**11-09-2018**
Vandaag zijn we begonnen met het duidelijk maken van onze rollen naar elkaar toe. De rollen waren al vastgesteld voor dat het schooljaar begon. Door deze rollen duidelijk te maken voor elkaar moesten we per rol opschrijven wat voor verwachtingen er bij zijn, zo weet elk persoon waar diegene aan moet voldoen.  
Daarnaast hebben punten opgeschreven die wij belangrijk vinden om te onderzoeken, deze hebben we ook genummerd op relevantie. 

Ik heb mijn eerste les van LABIAD gevolgd. Hier heb ik concurrentie onderzoek gedaan met een groepje en dit gepresenteerd voor de klas. Dit kon ik weer meenemen in ons project.

**14-09-2018**
Als vervolg op dinsdag zijn we begonnen met deskresearch, hier hebben we ook per persoon een gedeelte op ons genomen en informatie verzameld. Ik heb onderzoek gedaan naar verschillende woonsectoren. 
Ook hebben we besloten dat ik mee ga lopen met de huismeester en een teamgenoot gaat meelopen bij de balie.

**18-09-2018**
Vandaag hebben we ons teamuitje gepresenteerd aan de klas. 

Omdat we alleen een scrumboard hadden online hebben we er een fysiek gemaakt. Hiervoor hebben we gekozen zodat we snel kunnen kijken hoever we zijn in het proces. 
Vanuit het scrumboard heb ik een persoonlijke planning gemaakt zodat ik voor mezelf overzicht heb wat ik nog moet doen en in welke tijd.

Later op de dag hebben we het deskresearch doorgezet om meer informatie te verkrijgen over Woonstad en hun systeem. Ook hebben we overleg gehad over hoe we het onderzoek bij de huismeester gaan aanpakken. Ik heb hiervoor een vragenlijstje opgesteld en een inzichtenkaart waar de situaties in kunnen worden verwerkt.

**20-09-2018**
Vandaag heb ik meegelopen met de huismeester. Ik heb hem vragen gesteld en de situaties geëvalueerd. Dit heb ik gedaan door de inzichtkaarten in te vullen en de vragenlijst. 

**21-09-2018**
Ik ben de dag begonnen met vertellen waar ik achter ben gekomen bij de huismeester. De resultaten hebben we besproken. Nu afwachten wat er uit het onderzoek bij de balie komt en dan kunnen we verder met ons project. 

We hebben even georiënteerd wat we in onze onderzoeksposter willen verwerken. Zo kunnen we de deliverables verdelen onder elkaar.




**25-09-2018**
Vandaag hebben we de resultaten van de balie besproken. Vanuit deze resultaten hebben we waardes op kunnen schrijven en aan de hand daarvan heb ik een persona op kunnen stellen. Dit zorgt er voor dat er een duidelijk beeld is van onze doelgroep.

**27-09-2018** 
We zorgen er vandaag voor dat alle deliverables goed gemaakt zijn zodat ze morgen alleen nog maar in de poster moeten worden geplaatst.

**28-09-2018** 
Vandaag maken we de poster helemaal af samen met een feedback formulier. Zo kan het ingeleverd worden en gaat het naar Woonstad voor feedback. 

**SPRINT 2**
**09-10-2018**
Vandaag gaan we ons richten op creatieve technieken, op deze manier komen we op ideeën voor 3 mogelijke concepten. We hebben Blossom, Mash-up, Darkside en COCD box toegepast. We zijn op 3 concepten gekomen en werken deze thuis uit. 

We hebben de peerassessments besproken, waar onze zwakke punten als team nog liggen en die we kunnen verbeteren.

Thuis heb ik een scenario getekend van het concept

**12-10-2019**
We hebben een presentatie gehad van woonstad. Deze was een beetje verontrustend zonder enige medewerking. We hebben dit in de klas nog nabesproken.

**15-10-2018** 
Vandaag hebben we nog een keer de COCD box toegepast om op een beter concept te komen en meerdere ideeën.

**16-10-2018** 
We hebben feedback gehad op ons ontwerpverslag, deze was over het algemeen redelijk positief. De persona moest aangepast worden en nog een paar kleine dingen. Ik heb de persona meteen aangepast met Sanne door echt naar de waardes te kijken van de doelgroep. 

**19-10-2018**
Vandaag heb ik het scenario afgemaakt zodat mijn team de concepten kon presenteren aan woonstad. Het is ook de deadline dus alles is ingeleverd. 

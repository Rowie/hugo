---
Title: Dag 3 - Iteratie 1 
Date: 2017-11-21
---

Vandaag heb ik een hoorcollege gehad over 'De Mens'. Hier ging het over theorieën over de mens, waarom ze bepaalde dingen doen en waarom ze dat doen. Daarnaast heb ik mijn eerste les 'ondersteunend Nederlands' gehad, hier hebben we oefeningen gedaan over werkwoordspelling. Dat was weer even een opfrismomentje. 
Later op de dag heb ik Photoshop gehad, daarvoor had ik tussenuren waar ik het huiswerk voor deze les heb gemaakt. Deze les moesten we met meerdere lagen werken en neppe dingen echt laten lijken.
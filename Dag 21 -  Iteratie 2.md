---
Title: Dag 21 - Iteratie 2
Date: 2017-12-20
---
Vandaag de laatste projectdag voor de vakantie. Het was een beetje rommelig in de klas wat redelijk logisch is want er staat vanavond een kerstfeestje op de planning, georganiseerd door studenten van CMI! Maar ondanks dat heb ik vandaag een flowchart gemaakt, ik heb ook een poster gemaakt over ons concept en hier feedback over gevraagd aan mijn team. De feedback die ik kreeg was goed alleen ik moest laten uitstralen dat het over een feest ging dus heb ik die aanpassingen vervolgens ook gedaan. 
Ook heb ik een workshop gevolgd over **HTML-CSS** hier heb ik met *Atom* gewerkt en de beginstapjes geleerd over hoe je je website moet opbouwen.
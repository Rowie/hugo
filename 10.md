---
Title: Dag 11 - Iteratie 2
Date: 2017-09-18
---

Vandaag zijn we gestart met de presentaties over de games. We hebben onze game gepresenteerd aan de hele studio en aan de projectleiders. We hebben erg goede feedback gekregen!


Later in de middag zijn hebben we een begin gemaakt aan iteratie 2. De opdracht was "*kill your darling*", dat was wel even zuur want het houd in dat je het concept van iteratie 1 helemaal moet deleten. We moesten dus met een heel nieuw concept komen. Hiervoor hadden we natuurlijk weer meer onderzoek nodig en meerdere spelanalyses. We hebben het ene beetje besproken, en ik heb toen de planning gemaakt. Iedereen kreeg een aantal taken wat ze moesten doen voor de deliverables. Ik kreeg de taak om het moodboard te verbeteren en dieper onderzoek doen naar de doelgroep (CMD).
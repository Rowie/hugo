---
Title: Dag 1 - Iteratie 1
Date: 2017-09-04
---

Vandaag begonnen met de studio inrichten. Er zijn regels samengesteld voor ons team en voor de studio. Vervolgens is er gebrainstormd over wat we gaan onderzoeken, deze punten heb ik opgeschreven. Ik heb onderzoek gedaan naar welke studies er zijn op HR en welke eventueel interessant zijn als doelgroep. Daarnaast hebben we vragen bedacht voor een interview voor de studenten. Tussendoor hebben we een teamposter gemaakt met ambities, achtergrond, kwaliteiten en regels. Daarna is er een planning gemaakt voor de komende 2 weken.